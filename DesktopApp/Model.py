import tensorflow as tf
import numpy as np
from tensorflow import keras
import math


def predicton(Data):
    reconstructed_model = keras.models.load_model("my_model")
    prediction = abs(reconstructed_model.predict(Data)[0][0])
    return prediction
